package com.ynov.aja.schoolfinder.activities;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;

import com.ynov.aja.schoolfinder.R;
import com.ynov.aja.schoolfinder.enums.EnumSettings;
import com.ynov.aja.schoolfinder.services.RepositoryService;

public class SettingsActivity extends AppCompatActivity {

    private CheckBox publicSchoolCheckBox;
    private CheckBox privateSchoolCheckBox;

    private EnumSettings settings = EnumSettings.noneSchool;
    private Boolean canGoBack(){
        return (settings != EnumSettings.noneSchool);
    }

    private RepositoryService repositoryService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        prepareActionBar();
        getViewComponent();

        repositoryService = RepositoryService.getInstance(getApplicationContext());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        settings = getSettingsDependingOfCheckBox();
        if(canGoBack()){
            saveSelectedButton();
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return false;
    }

    @SuppressLint("ResourceType")
    private void prepareActionBar(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.SettingsActivityTitle));
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getString(R.color.yellowBackground))));
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void getViewComponent() {
        publicSchoolCheckBox = findViewById(R.id.publicSchoolCheckBox);
        privateSchoolCheckBox = findViewById(R.id.privateSchoolCheckBox);
    }

    private void saveSelectedButton(){
        if (settings == EnumSettings.noneSchool)
            return;

        repositoryService.saveSettings(settings.name());
    }

    private EnumSettings getSettingsDependingOfCheckBox() {
        if(publicSchoolCheckBox.isChecked() && privateSchoolCheckBox.isChecked())
            return EnumSettings.allSchool;
        else if(publicSchoolCheckBox.isChecked())
            return EnumSettings.publicSchool;
        else if(privateSchoolCheckBox.isChecked())
            return EnumSettings.privateSchool;
        else
            return EnumSettings.noneSchool;
    }
}