package com.ynov.aja.schoolfinder.exceptions;

public class RepositoryException extends Exception {

    public RepositoryException (String message)
    {
        super (message);
    }
}
