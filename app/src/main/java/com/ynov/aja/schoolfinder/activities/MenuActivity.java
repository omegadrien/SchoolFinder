package com.ynov.aja.schoolfinder.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.ynov.aja.schoolfinder.R;

public class MenuActivity extends AppCompatActivity {

    private ImageButton mapImageButton;
    private ImageButton viewSchoolImageButton;
    private ImageButton addSchoolImageButton;
    private ImageButton settingsImageButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        getViewComponent();
        setOnClickedListeners();
    }

    public void getViewComponent() {
        mapImageButton = findViewById(R.id.mapImageButton);
        viewSchoolImageButton = findViewById(R.id.viewSchoolImageButton);
        addSchoolImageButton = findViewById(R.id.addSchoolImageButton);
        settingsImageButton = findViewById(R.id.settingsImageButton);
    }

    private void setOnClickedListeners() {
        mapImageButton.setOnClickListener(genericOnClickListener);
        viewSchoolImageButton.setOnClickListener(genericOnClickListener);
        addSchoolImageButton.setOnClickListener(genericOnClickListener);
        settingsImageButton.setOnClickListener(genericOnClickListener);
    }

    private final View.OnClickListener genericOnClickListener = new View.OnClickListener(){
        public void onClick(final View v){

            switch (v.getId()){
                case R.id.mapImageButton:
                    goToMapsActivity();
                    break;
                case R.id.viewSchoolImageButton:
                    goToSchoolListActivity();
                    break;
                case R.id.addSchoolImageButton:
                    goToCreateSchoolActivity();
                    break;
                case R.id.settingsImageButton:
                    goToSettingsActivity();
                    break;
            }
        }
    };

    private void goToMapsActivity() {
        Intent intent = new Intent(MenuActivity.this, MapsActivity.class);
        startActivity(intent);
    }

    private void goToSchoolListActivity() {
        Intent intent = new Intent(MenuActivity.this, ListSchoolActivity.class);
        startActivity(intent);
    }

    private void goToCreateSchoolActivity() {
        Intent intent = new Intent(MenuActivity.this, CreateSchoolActivity.class);
        startActivity(intent);
    }

    private void goToSettingsActivity() {
        Intent intent = new Intent(MenuActivity.this, SettingsActivity.class);
        startActivity(intent);
    }
}
