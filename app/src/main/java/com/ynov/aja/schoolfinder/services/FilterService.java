package com.ynov.aja.schoolfinder.services;

import com.ynov.aja.schoolfinder.enums.EnumSettings;
import com.ynov.aja.schoolfinder.models.School;

import java.util.ArrayList;

public class FilterService {
    public static ArrayList<School> filterSchools(ArrayList<School> schools, String settings) {
        if(settings.equals(EnumSettings.allSchool.name()))
            return schools;

        ArrayList<School> filterSchools = new ArrayList<>();
        if(settings.equals(EnumSettings.publicSchool.name()))
            settings = "public";
        else
            settings = "privée";
        for(School s : schools) {
            if(s.getStatus().toLowerCase().equals(settings))
                filterSchools.add(s);
        }

        return filterSchools;
    }
}
