package com.ynov.aja.schoolfinder.models;

import com.google.gson.annotations.SerializedName;

public class LoginInfo {

    @SerializedName("email")
    private String email;

    @SerializedName("password")
    private String password;

    public LoginInfo(String email, String password){
        this.email = email;
        this.password = password;

    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return this.password;
    }
}
