package com.ynov.aja.schoolfinder.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.ynov.aja.schoolfinder.R;
import com.ynov.aja.schoolfinder.adapters.SchoolAdaptater;
import com.ynov.aja.schoolfinder.enums.EnumSettings;
import com.ynov.aja.schoolfinder.listeners.OnSchoolItemClickListener;
import com.ynov.aja.schoolfinder.models.School;
import com.ynov.aja.schoolfinder.models.SchoolResponse;
import com.ynov.aja.schoolfinder.services.ApiService;
import com.ynov.aja.schoolfinder.services.FilterService;
import com.ynov.aja.schoolfinder.services.IApiService;
import com.ynov.aja.schoolfinder.services.RepositoryService;

import java.util.ArrayList;
import java.util.List;

import im.delight.android.location.SimpleLocation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListSchoolActivity extends AppCompatActivity {

    private ArrayList<School> schools;
    private RecyclerView recyclerView;
    private SchoolAdaptater adaptater;

    private SimpleLocation location;

    private RepositoryService repositoryService;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_list);

        prepareActionBar();

        recyclerView = findViewById(R.id.schoolRecycleView);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 1));

        repositoryService = RepositoryService.getInstance(getApplicationContext());

        schools = new ArrayList<>();

        if (!checkLocationPermission()){
            Log.i("Error", "Not Authorization");
            return;
        }
        getPosition();
    }

    private void getPosition() {
        location = new SimpleLocation(this, false, false, 30 * 1000);

        if (!location.hasLocationEnabled()) {
            SimpleLocation.openSettings(this);
        }

        if(location.getPosition() != null){
            getSchools();
        }

        location.setListener(new SimpleLocation.Listener() {

            public void onPositionChanged() {
                getSchools();
            }

        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (checkLocationPermission())
            location.beginUpdates();
    }

    @Override
    protected void onPause() {
        if (checkLocationPermission())
            location.endUpdates();

        super.onPause();
    }

    @SuppressLint("ResourceType")
    private void prepareActionBar(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.ListSchoolActivityTitle));
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getString(R.color.orangeBackground))));
        actionBar.setDisplayHomeAsUpEnabled(true);
    }


    private void getSchools() {
        IApiService api = ApiService.getApiService();

        api.getSchools(repositoryService.loadToken()).enqueue(new Callback<SchoolResponse>() {
            @Override
            public void onResponse(Call<SchoolResponse> call, Response<SchoolResponse> response) {
                if (!response.isSuccessful() || response.body().getSchools() == null)
                {
                    Toast.makeText(ListSchoolActivity.this, getResources().getString(R.string.getSchoolsFailed), Toast.LENGTH_SHORT).show();
                }
                schools = FilterService.filterSchools(response.body().getSchools(), repositoryService.loadSettings());
                adaptater = new SchoolAdaptater(schools, schoolItemClickListener, location.getPosition());
                recyclerView.setAdapter(adaptater);
                Toast.makeText(ListSchoolActivity.this, getResources().getString(R.string.getSchoolsSuccess), Toast.LENGTH_SHORT).show(); //TODO
            }

            @Override
            public void onFailure(Call<SchoolResponse> call, Throwable t) {
                Toast.makeText(ListSchoolActivity.this, getResources().getString(R.string.getSchoolsFailed), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private OnSchoolItemClickListener schoolItemClickListener = new OnSchoolItemClickListener() {
        @Override
        public void onSchoolItemClick(View view, int position) {
            Intent intent = new Intent(ListSchoolActivity.this, DetailSchoolActivity.class);
            intent.putExtra(getString(R.string.schoolKey), schools.get(position));
            startActivity(intent);
        }

        @Override
        public void onViewMapSchoolClick(View view, int position) {
            Intent intent = new Intent(ListSchoolActivity.this, MapsActivity.class);
            intent.putExtra(getString(R.string.mapSchoolKey), schools.get(position));
            startActivity(intent);
        }

        @Override
        public void onDeleteSchoolClick(View view, int position) {
            deleteSelectedSchoolShowAlert(String.valueOf(schools.get(position).getId()));
        }
    };

    private void deleteSelectedSchoolShowAlert(final String currentSchoolId) {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.DeleteSchoolAlertTitle);
        alert.setMessage(R.string.DeleteSchoolAlertDescription);
        alert.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                deleteSelectedSchool(currentSchoolId);
            }
        });
        alert.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        alert.show();
    }

    private void deleteSelectedSchool(final String currentSchoolId) {
        IApiService api = ApiService.getApiService();
        api.deleteSchool(repositoryService.loadToken(), currentSchoolId).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (!response.isSuccessful())
                {
                    Toast.makeText(ListSchoolActivity.this, getResources().getString(R.string.deleteSchoolFailed), Toast.LENGTH_SHORT).show();
                }

                Integer pos = adaptater.getItemPosition(Integer.parseInt(currentSchoolId));
                adaptater.deleteItem(pos);
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(ListSchoolActivity.this, getResources().getString(R.string.deleteSchoolFailed), Toast.LENGTH_SHORT).show();;
            }
        });
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //https://stackoverflow.com/questions/40142331/how-to-request-location-permission-at-runtime-on-android-6
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        getPosition();
                    }

                } else {
                    // permission denied
                    finish();
                }
                return;
            }

        }
    }
}
