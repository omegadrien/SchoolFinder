package com.ynov.aja.schoolfinder.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.ynov.aja.schoolfinder.R;
import com.ynov.aja.schoolfinder.models.School;
import com.ynov.aja.schoolfinder.models.SchoolResponse;
import com.ynov.aja.schoolfinder.services.ApiService;
import com.ynov.aja.schoolfinder.services.FilterService;
import com.ynov.aja.schoolfinder.services.IApiService;
import com.ynov.aja.schoolfinder.services.RepositoryService;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import im.delight.android.location.SimpleLocation;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private ArrayList<School> schools;
    private Marker previousSelectedMarker;
    private Marker userPositionMarker;
    private School transferedSchoolFromOtherActivity;

    private TextView name;
    private TextView address;
    private TextView studentNumber;
    private TextView distance;
    private ImageView indicatorSchool;
    private ImageButton closeButton;
    private ImageButton centerUserPositionButton;

    private ConstraintLayout schoolLayout;

    private SimpleLocation location;
    private RepositoryService repositoryService;

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final int MARKER_ICON_REGULAR_SIZE = 150;
    private static final int MARKER_ICON_BIGGER_SIZE = 300;
    private static final int CAMERA_UPDATE_ZOOM = 15;
    private static final int CAMERA_CITY_ZOOM = 12;
    private static final int UPDATE_USER_POSITION_INTERVAL_30_SECONDS = 30 * 1000;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        getViewComponent();
        setOnClickedListeners();

        schoolLayout.setVisibility(View.INVISIBLE);

        repositoryService = RepositoryService.getInstance(getApplicationContext());
        location = new SimpleLocation(this, false, false, UPDATE_USER_POSITION_INTERVAL_30_SECONDS);
        getEntityDataFromPreviousActivity();
    }

    private void getPosition() {
        if (!checkLocationPermission()){
            return;
        }

        if (!location.hasLocationEnabled()) {
            SimpleLocation.openSettings(this);
        }

        if(location.getPosition() != null){
            if (transferedSchoolFromOtherActivity == null){
                updateUserPositionAndMoveOnIt();
            }
            else {
                updateTransferedSchoolPositionAndMoveOnIt();
            }
        }

        location.setListener(new SimpleLocation.Listener() {

            public void onPositionChanged() {
                updateUserPosition();
            }
        });
    }

    private void updateTransferedSchoolPositionAndMoveOnIt() {
        LatLng transferedSchoolPosition = new LatLng(
                transferedSchoolFromOtherActivity.getLatitude(),
                transferedSchoolFromOtherActivity.getLongitude());
        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(transferedSchoolPosition, CAMERA_UPDATE_ZOOM);
        mMap.animateCamera(location);
    }

    private void updateUserPositionAndMoveOnIt(){
        LatLng userPosition = updateUserPosition();

        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(userPosition, CAMERA_UPDATE_ZOOM);
        mMap.animateCamera(location);
    }

    private LatLng updateUserPosition() {

        if(userPositionMarker != null){
            userPositionMarker.remove();
        }

        LatLng userPosition = new LatLng(location.getLatitude(), location.getLongitude());
        userPositionMarker = mMap.addMarker(new MarkerOptions()
                .position(userPosition)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.my_pos_icone)));

        return userPosition;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (checkLocationPermission())
            location.beginUpdates();
    }

    @Override
    protected void onPause() {
        if (checkLocationPermission())
            location.endUpdates();

        super.onPause();
    }

    @SuppressLint("ResourceType")
    private void prepareActionBar(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.mapsActivityTitle));
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getString(R.color.greenBackground))));
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_maps_menu, menu);
        prepareActionBar();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_go_to_list_school:
                Intent intent = new Intent(MapsActivity.this, ListSchoolActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getViewComponent() {
        name = findViewById(R.id.nameSchoolMapTextView);
        address = findViewById(R.id.addressSchoolMapTextView);
        studentNumber = findViewById(R.id.studentNumberSchoolMapTextView);
        distance = findViewById(R.id.distanceSchoolMapTextView);
        indicatorSchool = findViewById(R.id.indicatorSchoolMapImageView);
        closeButton = findViewById(R.id.closeSchoolMapImageButton);
        centerUserPositionButton = findViewById(R.id.centerUserPositionButton);
        schoolLayout = findViewById(R.id.schoolMapLayout);
    }

    private void setOnClickedListeners() {
        closeButton.setOnClickListener(genericOnClickListener);
        centerUserPositionButton.setOnClickListener(genericOnClickListener);
    }

    private final View.OnClickListener genericOnClickListener = new View.OnClickListener(){
        public void onClick(final View v){
            switch (v.getId()){
                case R.id.closeSchoolMapImageButton:
                    if(previousSelectedMarker != null)
                        hideCurrentSelectedMarker(previousSelectedMarker);
                    break;
                case R.id.centerUserPositionButton:
                    updateUserPositionAndMoveOnIt();
                    break;
            }
        }
    };


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMarkerClickListener(this);
        try {
            getPosition();
            getSchools();
        } catch (Exception e) {
            Log.e("ERROR", e.getMessage());
        }
    }

    private Bitmap getGoodIcon(School school, int height, int width){
        BitmapDrawable iconRed = (BitmapDrawable)getResources().getDrawable(R.drawable.marker_icon_red);
        BitmapDrawable iconOrange = (BitmapDrawable)getResources().getDrawable(R.drawable.marker_icon_orange);
        BitmapDrawable iconGreen = (BitmapDrawable)getResources().getDrawable(R.drawable.marker_icon_green);

        if(school.getStudentNumber() < 50)
            return Bitmap.createScaledBitmap(iconRed.getBitmap(), width, height, false);
        if (school.getStudentNumber() < 200)
            return Bitmap.createScaledBitmap(iconOrange.getBitmap(), width, height, false);
        else
            return Bitmap.createScaledBitmap(iconGreen.getBitmap(), width, height, false);
    }

    private void addSchoolMarkersToMap(){
        for (School s : schools) {
            Marker m = addMarkerToMap(s, MARKER_ICON_REGULAR_SIZE, MARKER_ICON_REGULAR_SIZE);
            if(transferedSchoolFromOtherActivity != null && transferedSchoolFromOtherActivity.getId() == s.getId()){
                onMarkerClick(m);
            }
        }
    }

    private Marker addMarkerToMap(School school, int height, int width){

        LatLng lng = new LatLng(school.getLatitude(), school.getLongitude());
        BitmapDescriptor iconToUse = BitmapDescriptorFactory.fromBitmap(getGoodIcon(school, height, width));
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(lng)
                .title(school.getName())
                .icon(iconToUse));
        marker.setTag(schools.indexOf(school));

        return marker;
    }

    private void getSchools() {
        IApiService api = ApiService.getApiService();

        api.getSchools(repositoryService.loadToken()).enqueue(new Callback<SchoolResponse>() {
            @Override
            public void onResponse(Call<SchoolResponse> call, Response<SchoolResponse> response) {
                if (!response.isSuccessful() || response.body().getSchools() == null)
                {
                    Toast.makeText(MapsActivity.this, getResources().getString(R.string.getSchoolsFailed), Toast.LENGTH_SHORT).show();
                }

                schools = response.body().getSchools();
                schools = FilterService.filterSchools(schools, repositoryService.loadSettings());
                Toast.makeText(MapsActivity.this, getResources().getString(R.string.getSchoolsSuccess), Toast.LENGTH_SHORT).show();
                addSchoolMarkersToMap();
            }

            @Override
            public void onFailure(Call<SchoolResponse> call, Throwable t) {
                Toast.makeText(MapsActivity.this, getResources().getString(R.string.getSchoolsFailed), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onMarkerClick(final Marker marker) {
        if(marker.getTag() == null)
            return false;

        if(previousSelectedMarker != null && previousSelectedMarker.equals(marker)){
            hideCurrentSelectedMarker(marker);
            return false;
        }

        if(previousSelectedMarker != null){
            addMarkerToMap(schools.get((Integer) previousSelectedMarker.getTag()), MARKER_ICON_REGULAR_SIZE, MARKER_ICON_REGULAR_SIZE);
            previousSelectedMarker.remove();
        }

        previousSelectedMarker = addMarkerToMap(schools.get((Integer) marker.getTag()), MARKER_ICON_BIGGER_SIZE, MARKER_ICON_BIGGER_SIZE);

        schoolLayout.setVisibility(View.VISIBLE);
        setSchoolLayoutData(schools.get((Integer) marker.getTag()));

        marker.remove();

        return false;
    }

    private void hideCurrentSelectedMarker(Marker marker) {
        addMarkerToMap(schools.get((Integer) marker.getTag()), MARKER_ICON_REGULAR_SIZE, MARKER_ICON_REGULAR_SIZE);
        schoolLayout.setVisibility(View.INVISIBLE);

        previousSelectedMarker.remove();
        previousSelectedMarker = null;
    }

    private void setSchoolLayoutData(School school){
        name.setText(school.getName());
        address.setText(school.getAddress());
        studentNumber.setText(Integer.toString(school.getStudentNumber()) + " " + getResources().getString(R.string.students));

        double distanceInMeters = SimpleLocation.calculateDistance(location.getLatitude(), location.getLongitude(), school.getLatitude(), school.getLongitude());
        distance.setText(new DecimalFormat("#.##").format(distanceInMeters / 1000) + " Km");


        int studentNb = school.getStudentNumber();
        if(studentNb < 50){
            schoolLayout.setBackgroundColor(getResources().getColor(R.color.redMapBackground));
            indicatorSchool.setImageDrawable(getResources().getDrawable(R.drawable.ko_icone));
        }
        else if(studentNb < 200){
            schoolLayout.setBackgroundColor(getResources().getColor(R.color.orangeMapBackground));
            indicatorSchool.setImageDrawable(getResources().getDrawable(R.drawable.ok_icone));
        }

        else{
            schoolLayout.setBackgroundColor(getResources().getColor(R.color.greenMapBackground));
            indicatorSchool.setImageDrawable(getResources().getDrawable(R.drawable.ok_icone));
        }
    }

    private void getEntityDataFromPreviousActivity(){
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(getString(R.string.mapSchoolKey))) {
            transferedSchoolFromOtherActivity = (School)getIntent().getSerializableExtra(getString(R.string.mapSchoolKey));
        }
    }

    ///////////////////////////////////////////////////////////////////////////////////////////////////
    // inspired from https://stackoverflow.com/questions/20979592/dismiss-keyboard-when-edittext-loses-focus

    private static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager)  activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////
    // inspired from https://www.viralandroid.com/2016/04/google-maps-android-api-adding-search-bar-part-3.html

    public void onMapSearch(View view) {
        EditText locationSearch = findViewById(R.id.editText);
        String location = locationSearch.getText().toString();
        List<Address>addressList = null;

        if (location != null || !location.equals("")) {
            hideSoftKeyboard(MapsActivity.this);

            Geocoder geocoder = new Geocoder(this);
            try {
                addressList = geocoder.getFromLocationName(location, 1);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            if(addressList.isEmpty()){
                Toast.makeText(MapsActivity.this, getResources().getString(R.string.cityNotFound), Toast.LENGTH_SHORT).show();
                return;
            }

            Address address = addressList.get(0);
            LatLng latLng = new LatLng(address.getLatitude(), address.getLongitude());
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, CAMERA_CITY_ZOOM));
        }
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////
    //inspired from https://stackoverflow.com/questions/40142331/how-to-request-location-permission-at-runtime-on-android-6
    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // location-related task you need to do.
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        getPosition();
                    }

                } else {
                    // permission denied
                    finish();
                }
                return;
            }

        }
    }
}
