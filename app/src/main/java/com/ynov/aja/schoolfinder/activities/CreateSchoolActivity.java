package com.ynov.aja.schoolfinder.activities;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ynov.aja.schoolfinder.R;
import com.ynov.aja.schoolfinder.exceptions.RepositoryException;
import com.ynov.aja.schoolfinder.models.School;
import com.ynov.aja.schoolfinder.services.ApiService;
import com.ynov.aja.schoolfinder.services.IApiService;
import com.ynov.aja.schoolfinder.services.RepositoryService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateSchoolActivity extends AppCompatActivity {

    private EditText nameEditText;
    private EditText addressEditText;
    private EditText zipcodeEditText;
    private EditText townEditText;
    private EditText openingHoursEditText;
    private EditText phoneNumberEditText;
    private EditText emailEditText;
    private EditText latitudeEditText;
    private EditText longitudeEditText;
    private EditText studentNumberEditText;
    private Spinner schoolTypeSpinner;

    private Button submitButton;
    private Button cancelButton;

    private Boolean isBusy = false;

    private void setIsBusy(Boolean bool){
        isBusy = bool;
        toggleSubmitButtonEnable();
    }

    private Boolean getIsBusy(){
        return isBusy;
    }

    private RepositoryService repositoryService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_school);

        prepareActionBar();

        getViewComponent();
        setOnClickedListeners();

        repositoryService = RepositoryService.getInstance(getApplicationContext());
    }

    @SuppressLint("ResourceType")
    private void prepareActionBar(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.CreateSchoolActivityTitle));
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getString(R.color.purpleBackground))));
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void getViewComponent() {
        nameEditText = findViewById(R.id.nameCreateSchoolEditText);
        addressEditText = findViewById(R.id.addressCreateSchoolEditText);
        zipcodeEditText = findViewById(R.id.zipcodeCreateSchoolEditText);
        townEditText = findViewById(R.id.townCreateSchoolEditText);
        openingHoursEditText = findViewById(R.id.openingHourstownCreateSchoolEditText);
        phoneNumberEditText = findViewById(R.id.phoneNumberCreateSchoolEditText);
        emailEditText = findViewById(R.id.emailCreateSchoolEditText);
        latitudeEditText = findViewById(R.id.latitudeCreateSchoolEditText);
        longitudeEditText = findViewById(R.id.longitudeCreateSchoolEditText);
        studentNumberEditText = findViewById(R.id.studentCreateSchoolEditText);
        schoolTypeSpinner = findViewById(R.id.typeCreateSchoolSpinner);
        submitButton = findViewById(R.id.submitCreateSchoolButton);
        cancelButton = findViewById(R.id.cancelCreateSchoolButton);
    }

    private void setOnClickedListeners() {
        submitButton.setOnClickListener(genericOnClickListener);
        cancelButton.setOnClickListener(genericOnClickListener);
    }

    private final View.OnClickListener genericOnClickListener = new View.OnClickListener(){
        public void onClick(final View v){
            switch (v.getId()){
                case R.id.submitCreateSchoolButton:
                    onSubmitButtonClicked();
                    break;
                case R.id.cancelCreateSchoolButton:
                    onCancelButtonClicked();
                    break;
            }
        }
    };

    private void toggleSubmitButtonEnable() {
        submitButton.setEnabled(!submitButton.isEnabled());
    }

    private void onSubmitButtonClicked() {
        if(!isEntriesValid() || getIsBusy())
            return;

        setIsBusy(true);
        try {
            createSchool();
        }
        catch (Exception e){
            Log.e("ERROR", e.getMessage());
        }
    }

    private void onCancelButtonClicked() {
        returnToMenu();
    }

    private boolean isEntriesValid() {
        boolean isNameEntryEmpty = TextUtils.isEmpty(nameEditText.getText());
        boolean isAddressEntryEmpty = TextUtils.isEmpty(addressEditText.getText());
        boolean isZipcodeEntryEmpty = TextUtils.isEmpty(zipcodeEditText.getText());
        boolean isTownEntryEmpty = TextUtils.isEmpty(townEditText.getText());
        boolean isOpeningHoursEntryEmpty = TextUtils.isEmpty(openingHoursEditText.getText());
        boolean isPhoneNumberEntryEmpty = TextUtils.isEmpty(phoneNumberEditText.getText());
        boolean isEmailEntryEmpty = TextUtils.isEmpty(emailEditText.getText());
        boolean isEmailValid = isValidEmail(emailEditText.getText());
        boolean isLatitudeEntryEmpty = TextUtils.isEmpty(latitudeEditText.getText());
        boolean isLongitudeEntryEmpty = TextUtils.isEmpty(longitudeEditText.getText());
        boolean isStudentNumberEmpty = TextUtils.isEmpty(studentNumberEditText.getText());

        if(isNameEntryEmpty)
            nameEditText.setError(getText(R.string.nameCreateSchoolNeeded));

        if(isAddressEntryEmpty)
            addressEditText.setError(getText(R.string.addressCreateSchoolNeeded));

        if(isZipcodeEntryEmpty)
            zipcodeEditText.setError(getText(R.string.zipcodeCreateSchoolNeeded));

        if(isTownEntryEmpty)
            townEditText.setError(getText(R.string.townCreateSchoolNeeded));

        if(isOpeningHoursEntryEmpty)
            openingHoursEditText.setError(getText(R.string.openingHoursCreateSchoolNeeded));

        if(isPhoneNumberEntryEmpty)
            phoneNumberEditText.setError(getText(R.string.phoneNumberCreateSchoolNeeded));

        if(isEmailEntryEmpty)
            emailEditText.setError(getText(R.string.emailCreateSchoolNeeded));

        if(!isEmailValid)
            emailEditText.setError(getText(R.string.emailCreateSchoolInvalid));

        if(isLatitudeEntryEmpty)
            latitudeEditText.setError(getText(R.string.latitudeCreateSchoolNeeded));

        if(isLongitudeEntryEmpty)
            longitudeEditText.setError(getText(R.string.longitudeCreateSchoolNeeded));

        if(isStudentNumberEmpty)
            studentNumberEditText.setError(getText(R.string.studentNumberCreateSchoolNeeded));

        return !isNameEntryEmpty &&
                !isAddressEntryEmpty &&
                !isZipcodeEntryEmpty &&
                !isTownEntryEmpty &&
                !isOpeningHoursEntryEmpty &&
                !isPhoneNumberEntryEmpty &&
                !isEmailEntryEmpty &&
                isEmailValid &&
                !isLatitudeEntryEmpty &&
                !isLongitudeEntryEmpty &&
                !isStudentNumberEmpty;
    }

    // https://stackoverflow.com/a/15245095
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void createSchool() throws RepositoryException {
        IApiService api = ApiService.getApiService();

        Gson gson = new Gson();
        String json = gson.toJson(createSchoolFromEditText());

        api.createSchool(repositoryService.loadToken(), json).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                setIsBusy(false);
                if(response.isSuccessful()) {
                    Toast.makeText(CreateSchoolActivity.this, getResources().getString(R.string.createSchoolSuccess), Toast.LENGTH_SHORT).show(); //TODO
                    returnToMenu();
                }
                else {
                    Toast.makeText(CreateSchoolActivity.this, getResources().getString(R.string.createSchoolFailed), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                setIsBusy(false);
                Toast.makeText(CreateSchoolActivity.this, getResources().getString(R.string.createSchoolFailed), Toast.LENGTH_SHORT).show(); //TODO
            }
        });
    }

    private School createSchoolFromEditText(){
        School school = new School();
        school.setName(nameEditText.getText().toString());
        school.setAddress(addressEditText.getText().toString());
        school.setZipcode(zipcodeEditText.getText().toString());
        school.setCity(townEditText.getText().toString());
        school.setOpeningHours(openingHoursEditText.getText().toString());
        school.setPhoneNumber(phoneNumberEditText.getText().toString());
        school.setEmail(emailEditText.getText().toString());
        school.setStatus(schoolTypeSpinner.getSelectedItem().toString());

        try{
            school.setLatitude(Double.parseDouble(latitudeEditText.getText().toString()));
            school.setLongitude(Double.parseDouble(longitudeEditText.getText().toString()));
            school.setStudentNumber(Integer.parseInt(studentNumberEditText.getText().toString()));
        }
        catch (Exception e){
            Log.e("ERROR", e.getMessage());
        }

        return school;
    }

    private void returnToMenu() {
        finish();
    }
}
