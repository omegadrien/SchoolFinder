package com.ynov.aja.schoolfinder.services;

import com.ynov.aja.schoolfinder.services.IApiService;
import com.ynov.aja.schoolfinder.services.RetrofitClient;

public class ApiService {

    private static  final String BASE_URL = "https://arcane-taiga-82700.herokuapp.com/";

    public static IApiService getApiService() {
        return RetrofitClient.getClient(BASE_URL).create(IApiService.class);
    }
}
