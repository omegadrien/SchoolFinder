package com.ynov.aja.schoolfinder.enums;

public enum EnumSettings {
    noneSchool, publicSchool, privateSchool, allSchool
}