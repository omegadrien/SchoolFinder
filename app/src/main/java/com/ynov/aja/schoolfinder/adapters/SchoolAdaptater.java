package com.ynov.aja.schoolfinder.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ynov.aja.schoolfinder.R;
import com.ynov.aja.schoolfinder.holders.SchoolHolder;
import com.ynov.aja.schoolfinder.listeners.OnSchoolItemClickListener;
import com.ynov.aja.schoolfinder.models.School;

import java.util.ArrayList;

import im.delight.android.location.SimpleLocation;

//SimpleAdapter(private val items: MutableList<String>) : RecyclerView.Adapter<SimpleAdapter.VH>() {
public class SchoolAdaptater extends RecyclerView.Adapter<SchoolHolder> {

    private ArrayList<School> list;
    private OnSchoolItemClickListener listener;
    private SimpleLocation.Point coord;

    public SchoolAdaptater(ArrayList<School> list, OnSchoolItemClickListener listener, SimpleLocation.Point coord){
        this.list = list;
        this.listener = listener;
        this.coord = coord;
    }

    @Override
    public SchoolHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.school_cell,viewGroup,false);
        return new SchoolHolder(view, listener);
    }

    @Override
    public void onBindViewHolder(SchoolHolder holder, int position) {
        School school = list.get(position);
        holder.bind(school, coord);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public int getItemPosition(int id)
    {
        for (int position=0; position<list.size(); position++)
            if (list.get(position).getId() == id)
                return position;
        return 0;
    }

    public void deleteItem(int position) {
        list.remove(position);
        notifyItemRemoved(position);
    }
}
