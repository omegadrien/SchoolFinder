package com.ynov.aja.schoolfinder.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ynov.aja.schoolfinder.R;
import com.ynov.aja.schoolfinder.exceptions.RepositoryException;
import com.ynov.aja.schoolfinder.models.School;
import com.ynov.aja.schoolfinder.services.ApiService;
import com.ynov.aja.schoolfinder.services.IApiService;
import com.ynov.aja.schoolfinder.services.RepositoryService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditSchoolActivity extends AppCompatActivity {

    private School school;

    private EditText nameEditText;
    private EditText addressEditText;
    private EditText zipcodeEditText;
    private EditText townEditText;
    private EditText openingHoursEditText;
    private EditText phoneNumberEditText;
    private EditText emailEditText;
    private EditText latitudeEditText;
    private EditText longitudeEditText;
    private EditText studentNumberEditText;
    private Spinner schoolTypeSpinner;

    private Button submitButton;
    private Button cancelButton;

    private Boolean isBusy = false;

    private void setIsBusy(Boolean bool){
        isBusy = bool;
        toggleSubmitButtonEnable();
    }

    private Boolean getIsBusy(){
        return isBusy;
    }

    private RepositoryService repositoryService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_school);

        getViewComponent();
        setOnClickedListeners();

        repositoryService = RepositoryService.getInstance(getApplicationContext());

        getEntityDataFromPreviousActivity();
        populateViewComponentWithEntryData();
    }

    @SuppressLint("ResourceType")
    private void prepareActionBar(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.EditSchoolActivityTitle) + " " + nameEditText.getText().toString());
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getString(R.color.orangeBackground))));
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_edit_menu, menu);
        prepareActionBar();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_return_to_detail:
                returnToDetailPage();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getViewComponent() {
        nameEditText = findViewById(R.id.nameEditSchoolEditText);
        addressEditText = findViewById(R.id.addressEditSchoolEditText);
        zipcodeEditText = findViewById(R.id.zipcodeEditSchoolEditText);
        townEditText = findViewById(R.id.townEditSchoolEditText);
        openingHoursEditText = findViewById(R.id.openingHourstownEditSchoolEditText);
        phoneNumberEditText = findViewById(R.id.phoneNumberEditSchoolEditText);
        emailEditText = findViewById(R.id.emailEditSchoolEditText);
        latitudeEditText = findViewById(R.id.latitudeEditSchoolEditText);
        longitudeEditText = findViewById(R.id.longitudeEditSchoolEditText);
        studentNumberEditText = findViewById(R.id.studentEditSchoolEditText);
        schoolTypeSpinner = findViewById(R.id.typeEditSchoolSpinner);

        submitButton = findViewById(R.id.submitEditSchool);
        cancelButton = findViewById(R.id.cancelEditSchool);
    }

    private void setOnClickedListeners() {
        submitButton.setOnClickListener(genericOnClickListener);
        cancelButton.setOnClickListener(genericOnClickListener);
    }

    private final View.OnClickListener genericOnClickListener = new View.OnClickListener(){
        public void onClick(final View v){
            switch (v.getId()){
                case R.id.submitEditSchool:
                    onSubmitButtonClicked();
                    break;
                case R.id.cancelEditSchool:
                    onCancelButtonClicked();
                    break;
            }
        }
    };

    private void getEntityDataFromPreviousActivity(){
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(getString(R.string.schoolKey))) {
            school = (School)getIntent().getSerializableExtra(getString(R.string.schoolKey));
        }
    }

    private void populateViewComponentWithEntryData(){
        if(school == null)
            return;

        nameEditText.setText(school.getName());
        addressEditText.setText(school.getAddress());
        zipcodeEditText.setText(school.getZipcode());
        townEditText.setText(school.getCity());
        openingHoursEditText.setText(school.getOpeningHours());
        phoneNumberEditText.setText(school.getPhoneNumber());
        emailEditText.setText(school.getEmail());

        latitudeEditText.setText(Double.toString(school.getLatitude()));
        longitudeEditText.setText(Double.toString(school.getLongitude()));
        studentNumberEditText.setText(String.valueOf(school.getStudentNumber()));

        if(school.getStatus().equals("public"))
            schoolTypeSpinner.setSelection(0);
        else
            schoolTypeSpinner.setSelection(1);
    }

    private boolean isEntriesValid() {
        boolean isNameEntryEmpty = TextUtils.isEmpty(nameEditText.getText());
        boolean isAddressEntryEmpty = TextUtils.isEmpty(addressEditText.getText());
        boolean isZipcodeEntryEmpty = TextUtils.isEmpty(zipcodeEditText.getText());
        boolean isTownEntryEmpty = TextUtils.isEmpty(townEditText.getText());
        boolean isOpeningHoursEntryEmpty = TextUtils.isEmpty(openingHoursEditText.getText());
        boolean isPhoneNumberEntryEmpty = TextUtils.isEmpty(phoneNumberEditText.getText());
        boolean isEmailEntryEmpty = TextUtils.isEmpty(emailEditText.getText());
        boolean isEmailValid = isValidEmail(emailEditText.getText());
        boolean isLatitudeEntryEmpty = TextUtils.isEmpty(latitudeEditText.getText());
        boolean isLongitudeEntryEmpty = TextUtils.isEmpty(longitudeEditText.getText());
        boolean isStudentNumberEmpty = TextUtils.isEmpty(studentNumberEditText.getText());

        if(isNameEntryEmpty)
            nameEditText.setError(getText(R.string.nameCreateSchoolNeeded));

        if(isAddressEntryEmpty)
            addressEditText.setError(getText(R.string.addressCreateSchoolNeeded));

        if(isZipcodeEntryEmpty)
            zipcodeEditText.setError(getText(R.string.zipcodeCreateSchoolNeeded));

        if(isTownEntryEmpty)
            townEditText.setError(getText(R.string.townCreateSchoolNeeded));

        if(isOpeningHoursEntryEmpty)
            openingHoursEditText.setError(getText(R.string.openingHoursCreateSchoolNeeded));

        if(isPhoneNumberEntryEmpty)
            phoneNumberEditText.setError(getText(R.string.phoneNumberCreateSchoolNeeded));

        if(isEmailEntryEmpty)
            emailEditText.setError(getText(R.string.emailCreateSchoolNeeded));

        if(!isEmailValid)
            emailEditText.setError(getText(R.string.emailCreateSchoolInvalid));

        if(isLatitudeEntryEmpty)
            latitudeEditText.setError(getText(R.string.latitudeCreateSchoolNeeded));

        if(isLongitudeEntryEmpty)
            longitudeEditText.setError(getText(R.string.longitudeCreateSchoolNeeded));

        if(isStudentNumberEmpty)
            studentNumberEditText.setError(getText(R.string.studentNumberCreateSchoolNeeded));

        return !isNameEntryEmpty &&
                !isAddressEntryEmpty &&
                !isZipcodeEntryEmpty &&
                !isTownEntryEmpty &&
                !isOpeningHoursEntryEmpty &&
                !isPhoneNumberEntryEmpty &&
                !isEmailEntryEmpty &&
                isEmailValid &&
                !isLatitudeEntryEmpty &&
                !isLongitudeEntryEmpty &&
                !isStudentNumberEmpty;
    }

    // https://stackoverflow.com/a/15245095
    public final static boolean isValidEmail(CharSequence target) {
        if (target == null)
            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    private void toggleSubmitButtonEnable() {
        submitButton.setEnabled(!submitButton.isEnabled());
    }

    private void onSubmitButtonClicked() {
        if(!isEntriesValid() || getIsBusy())
            return;

        setIsBusy(true);
        try {
            editSchool();
        }
        catch (Exception e){
            Log.e("ERROR", e.getMessage());
        }
    }

    private void onCancelButtonClicked() {
        returnToDetailPage();
    }

    private void editSchool() throws RepositoryException {
        IApiService api = ApiService.getApiService();

        Gson gson = new Gson();
        String json = gson.toJson(createSchoolFromEditText());

        api.updateSchool(repositoryService.loadToken(), Integer.toString(school.getId()), json).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                setIsBusy(false);
                if(response.isSuccessful()) {
                    Toast.makeText(EditSchoolActivity.this, getResources().getString(R.string.updateSchoolSuccess), Toast.LENGTH_SHORT).show();
                    returnToListPage();
                }
                else{
                    Toast.makeText(EditSchoolActivity.this, getResources().getString(R.string.updateSchoolFailed), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                setIsBusy(false);
                Toast.makeText(EditSchoolActivity.this, getResources().getString(R.string.updateSchoolFailed), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private School createSchoolFromEditText(){
        School school = new School(this.school.getId(),
                nameEditText.getText().toString(),
                addressEditText.getText().toString(),
                zipcodeEditText.getText().toString(),
                townEditText.getText().toString(),
                openingHoursEditText.getText().toString(),
                phoneNumberEditText.getText().toString(),
                emailEditText.getText().toString(),
                Double.parseDouble(latitudeEditText.getText().toString()),
                Double.parseDouble(longitudeEditText.getText().toString()),
                Integer.parseInt(studentNumberEditText.getText().toString()),
                schoolTypeSpinner.getSelectedItem().toString());

        return school;
    }

    private void returnToDetailPage() {
        finish();
    }

    private void returnToListPage(){
        Intent intent = new Intent(EditSchoolActivity.this, ListSchoolActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
