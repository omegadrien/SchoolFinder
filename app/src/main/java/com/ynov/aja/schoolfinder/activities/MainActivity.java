package com.ynov.aja.schoolfinder.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ynov.aja.schoolfinder.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}
