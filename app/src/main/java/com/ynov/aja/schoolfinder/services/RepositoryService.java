package com.ynov.aja.schoolfinder.services;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.ynov.aja.schoolfinder.R;
import com.ynov.aja.schoolfinder.enums.EnumSettings;
import com.ynov.aja.schoolfinder.exceptions.RepositoryException;
import com.ynov.aja.schoolfinder.models.LoginInfo;

public class RepositoryService {

    private static RepositoryService repositoryService = new RepositoryService();
    private static SharedPreferences prefs;

    private static final String TOKEN_KEY = "token";
    private static final String LOGIN_KEY = "login";
    private static final String PASSWORD_KEY = "password";
    private static final String CONFIGURATION_KEY = "configuration";

    private RepositoryService() {}

    //The context passed into the getInstance should be application level context.
    public static RepositoryService getInstance(Context context) {
        if (prefs == null) {
            prefs = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
        }
        return repositoryService;
    }

    public String loadToken() {
        String token = prefs.getString(TOKEN_KEY, "");
        return token;
    }

    public void saveToken(String token){
        prefs.edit().putString(TOKEN_KEY, token).apply();
    }

    public LoginInfo loadLoginInfo(){
        String email = prefs.getString(LOGIN_KEY, "");
        String password = prefs.getString(PASSWORD_KEY, "");
        return new LoginInfo(email, password);
    }

    public void saveLoginInfo(LoginInfo info) {
        prefs.edit()
                .putString(LOGIN_KEY, info.getEmail())
                .putString(PASSWORD_KEY, info.getPassword())
                .apply();
    }

    public String loadSettings(){
        return prefs.getString(CONFIGURATION_KEY, EnumSettings.allSchool.name());
    }

    public void saveSettings(String settings){
        prefs.edit()
            .putString(CONFIGURATION_KEY, settings)
            .apply();
    }
}
