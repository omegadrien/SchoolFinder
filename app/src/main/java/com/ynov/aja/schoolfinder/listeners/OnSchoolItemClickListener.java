package com.ynov.aja.schoolfinder.listeners;

import android.view.View;

public interface OnSchoolItemClickListener {
    public void onSchoolItemClick(View view, int position);
    public void onViewMapSchoolClick(View view, int position);
    public void onDeleteSchoolClick(View view, int position);
}
