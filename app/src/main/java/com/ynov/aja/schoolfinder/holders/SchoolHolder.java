package com.ynov.aja.schoolfinder.holders;

import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ynov.aja.schoolfinder.R;
import com.ynov.aja.schoolfinder.listeners.OnSchoolItemClickListener;
import com.ynov.aja.schoolfinder.models.School;
import com.ynov.aja.schoolfinder.services.DistanceService;

import java.text.DecimalFormat;

import im.delight.android.location.SimpleLocation;

public class SchoolHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private TextView name;
    private TextView address;
    private TextView studentNumber;
    private ImageView image;
    private ImageButton removeSchoolButton;
    private ImageButton viewMapButton;

    private TextView distanceTextView;

    private OnSchoolItemClickListener listener;

    private DistanceService distanceService = new DistanceService();

    public SchoolHolder(View itemView, OnSchoolItemClickListener listener)
    {
        super(itemView);

        name = itemView.findViewById(R.id.nameSchoolCellTextView);
        address = itemView.findViewById(R.id.addressSchoolCellTextView);
        studentNumber = itemView.findViewById(R.id.studentNumberSchoolCellTextView);
        image = itemView.findViewById(R.id.indicatorSchoolCellImageView);
        removeSchoolButton = itemView.findViewById(R.id.removeSchoolCellImageButton);
        viewMapButton = itemView.findViewById(R.id.mapSchoolCellImageButton);
        distanceTextView = itemView.findViewById(R.id.distanceSchoolCellTextView);

        removeSchoolButton.setOnClickListener(this);
        viewMapButton.setOnClickListener(this);

        this.listener = listener;
        itemView.setOnClickListener(this);

    }

    public void bind(School school, SimpleLocation.Point coord){
        name.setText(school.getName());
        address.setText(school.getAddress());
        studentNumber.setText(String.valueOf(school.getStudentNumber()) + " " +  itemView.getContext().getString(R.string.students));

        int studentNb = school.getStudentNumber();
        if(studentNb < 50){
            itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.redBackground));
            image.setImageDrawable(itemView.getResources().getDrawable(R.drawable.ko_icone));
        }
        else if(studentNb < 200)
            itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.orangeBackground));
        else
            itemView.setBackgroundColor(itemView.getContext().getResources().getColor(R.color.greenBackground));

        double distance = distanceService.distanceInKmBetweenEarthCoordinates(school.getLatitude(), school.getLongitude(), coord.latitude, coord.longitude);
        distanceTextView.setText(new DecimalFormat("#.##").format(distance) + " Km");
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.removeSchoolCellImageButton)
            listener.onDeleteSchoolClick(view, getAdapterPosition());
        else if (view.getId() == R.id.mapSchoolCellImageButton)
            listener.onViewMapSchoolClick(view, getAdapterPosition());
        else
            listener.onSchoolItemClick(view, getAdapterPosition());
    }
}
