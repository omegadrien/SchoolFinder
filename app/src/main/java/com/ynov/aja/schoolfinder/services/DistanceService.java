package com.ynov.aja.schoolfinder.services;

// Adapted from: https://stackoverflow.com/questions/365826/calculate-distance-between-2-gps-coordinates

import android.util.Log;

import im.delight.android.location.SimpleLocation;

public class DistanceService {

    private double degreesToRadians(double degrees) {
        return degrees * Math.PI / 180;
    }

    public double distanceInKmBeteweenYnovAndEarthCoordinates(double lat, double lon){
        double ynovLat = 45.7456831;
        double ynovLon = 4.83759340000006;

        return distanceInKmBetweenEarthCoordinates(lat, lon, ynovLat, ynovLon);
    }

    public double distanceInKmBetweenEarthCoordinates(double lat1, double lon1, double lat2, double lon2) {
        return SimpleLocation.calculateDistance(lat1, lon1, lat2, lon2) / 1000;
    }
}
