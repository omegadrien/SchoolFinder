package com.ynov.aja.schoolfinder.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.MenuBuilder;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.ynov.aja.schoolfinder.R;
import com.ynov.aja.schoolfinder.models.School;

public class DetailSchoolActivity extends AppCompatActivity {

    private School school;

    private EditText nameEditText;
    private EditText addressEditText;
    private EditText zipcodeEditText;
    private EditText townEditText;
    private EditText openingHoursEditText;
    private EditText phoneNumberEditText;
    private EditText emailEditText;
    private EditText latitudeEditText;
    private EditText longitudeEditText;
    private EditText studentNumberEditText;
    private Spinner schoolTypeSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_school);

        getViewComponent();

        getEntityDataFromPreviousActivity();
        populateViewComponentWithEntryData();
        disableSpinner();
    }

    // https://stackoverflow.com/questions/43916602/how-can-i-disable-spinner-in-android-and-enable-it-on-button-click
    // We have to disable it in the code
    private void disableSpinner() {
        schoolTypeSpinner.setEnabled(false);
    }

    @SuppressLint("ResourceType")
    private void prepareActionBar(){
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getString(R.string.DetailSchoolActivityTitle) + " " + nameEditText.getText().toString());
        actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor(getString(R.color.orangeBackground))));
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @SuppressLint("RestrictedApi")
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.actionbar_detail_menu, menu);
        if(menu instanceof MenuBuilder){
            MenuBuilder m = (MenuBuilder) menu;
            //noinspection RestrictedApi
            m.setOptionalIconsVisible(true);
        }
        prepareActionBar();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_map:
                goToMapPage();
                return true;
            case R.id.action_edit:
                goToEditSchoolPage();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void getViewComponent() {
        nameEditText = findViewById(R.id.nameDetailSchoolEditText);
        addressEditText = findViewById(R.id.addressDetailSchoolEditText);
        zipcodeEditText = findViewById(R.id.zipcodeDetailSchoolEditText);
        townEditText = findViewById(R.id.townDetailSchoolEditText);
        openingHoursEditText = findViewById(R.id.openingHourstownDetailSchoolEditText);
        phoneNumberEditText = findViewById(R.id.phoneNumberDetailSchoolEditText);
        emailEditText = findViewById(R.id.emailDetailSchoolEditText);
        latitudeEditText = findViewById(R.id.latitudeDetailSchoolEditText);
        longitudeEditText = findViewById(R.id.longitudeDetailSchoolEditText);
        studentNumberEditText = findViewById(R.id.studentDetailSchoolEditText);
        schoolTypeSpinner = findViewById(R.id.typeDetailSchoolSpinner);

    }

    private void getEntityDataFromPreviousActivity(){
        Bundle extras = getIntent().getExtras();
        if (extras != null && extras.containsKey(getString(R.string.schoolKey))) {
            school = (School)getIntent().getSerializableExtra(getString(R.string.schoolKey));
        }
    }

    private void populateViewComponentWithEntryData(){
        if(school == null)
            return;

        nameEditText.setText(school.getName());
        addressEditText.setText(school.getAddress());
        zipcodeEditText.setText(school.getZipcode());
        townEditText.setText(school.getCity());
        openingHoursEditText.setText(school.getOpeningHours());
        phoneNumberEditText.setText(school.getPhoneNumber());
        emailEditText.setText(school.getEmail());

        latitudeEditText.setText(Double.toString(school.getLatitude()));
        longitudeEditText.setText(Double.toString(school.getLongitude()));
        studentNumberEditText.setText(String.valueOf(school.getStudentNumber()));

        if(school.getStatus().equals("public"))
            schoolTypeSpinner.setSelection(0);
        else
            schoolTypeSpinner.setSelection(1);
    }

    private void goToMapPage() {
        Intent intent = new Intent(DetailSchoolActivity.this, MapsActivity.class);
        intent.putExtra(getString(R.string.mapSchoolKey), school);
        startActivity(intent);
    }

    private void goToEditSchoolPage() {
        Intent intent = new Intent(DetailSchoolActivity.this, EditSchoolActivity.class);
        intent.putExtra(getString(R.string.schoolKey), school);
        startActivity(intent);
    }
}
