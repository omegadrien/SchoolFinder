package com.ynov.aja.schoolfinder.services;

import com.ynov.aja.schoolfinder.models.AuthenticationResponse;
import com.ynov.aja.schoolfinder.models.School;
import com.ynov.aja.schoolfinder.models.SchoolResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface IApiService {

    @Headers("Content-Type: application/json")
    @POST("api/v1/users/sign_in")
    Call<AuthenticationResponse> authenticate(@Body String body);

    @Headers("Content-Type: application/json")
    @POST("/api/v1/schools")
    Call<String> createSchool(@Header("Authorization") String token, @Body String body);

    @Headers("Content-Type: application/json")
    @PATCH("api/v1/schools/{id}")
    Call<String> updateSchool(@Header("Authorization") String token, @Path("id") String id, @Body String body);

    @Headers("Content-Type: application/json")
    @DELETE("api/v1/schools/{id}")
    Call<String> deleteSchool(@Header("Authorization") String token, @Path("id") String id);

    @Headers("Content-Type: application/json")
    @GET("/api/v1/schools")
    Call<SchoolResponse> getSchools(@Header("Authorization") String token); //https://medium.com/@dds861/json-parsing-using-retrofit-and-recycleview-2300d9fdcf15
}
