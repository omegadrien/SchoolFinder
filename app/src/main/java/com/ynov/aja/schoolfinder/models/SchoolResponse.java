package com.ynov.aja.schoolfinder.models;

import java.util.ArrayList;

public class SchoolResponse {
    private ArrayList<School> schools;

    public ArrayList<School> getSchools() {
        return schools;
    }
}