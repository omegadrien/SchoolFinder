package com.ynov.aja.schoolfinder.models;

import com.google.gson.annotations.SerializedName;

public class AuthenticationResponse {

    @SerializedName("success")
    public boolean success;

    @SerializedName("auth_token")
    public String token;
}
