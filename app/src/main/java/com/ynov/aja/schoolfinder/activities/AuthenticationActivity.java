package com.ynov.aja.schoolfinder.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.ynov.aja.schoolfinder.R;
import com.ynov.aja.schoolfinder.models.AuthenticationResponse;
import com.ynov.aja.schoolfinder.models.LoginInfo;
import com.ynov.aja.schoolfinder.services.ApiService;
import com.ynov.aja.schoolfinder.services.IApiService;
import com.ynov.aja.schoolfinder.services.RepositoryService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthenticationActivity extends AppCompatActivity {

    private EditText loginEditText;
    private EditText passwordEditText;
    private Button clearButton;
    private Button sendButton;
    private CheckBox rememberMeCheckBox;

    private Boolean isBusy = false;

    private void setIsBusy(Boolean bool){
        isBusy = bool;
        updateButtonEnabled();
        updateEditTextEnabled();
        rememberMeCheckBox.setEnabled(!isBusy);
    }

    private Boolean getIsBusy(){
        return isBusy;
    }

    private void updateButtonEnabled() {
        sendButton.setEnabled(!isBusy);
        clearButton.setEnabled(!isBusy);
    }

    private void updateEditTextEnabled(){
        loginEditText.setEnabled(!isBusy);
        passwordEditText.setEnabled(!isBusy);
    }

    private RepositoryService repositoryService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentification);

        getViewComponent();
        setOnClickedListeners();

        repositoryService = RepositoryService.getInstance(getApplicationContext());

        setLoginAndPasswordTextViewUsingSavedData();
    }

    private void setOnClickedListeners() {
        clearButton.setOnClickListener(genericOnClickListener);
        sendButton.setOnClickListener(genericOnClickListener);
    }

    private void getViewComponent() {
        loginEditText = findViewById(R.id.usernameEntry);
        passwordEditText = findViewById(R.id.passwordEntry);
        clearButton = findViewById(R.id.clearButton);
        sendButton = findViewById(R.id.sendButton);
        rememberMeCheckBox = findViewById(R.id.rememberMeCheckBox);
    }

    private void setLoginAndPasswordTextViewUsingSavedData() {
        LoginInfo loginInfo = repositoryService.loadLoginInfo();
        loginEditText.setText(loginInfo.getEmail());
        passwordEditText.setText(loginInfo.getPassword());
    }

    private final View.OnClickListener genericOnClickListener = new View.OnClickListener(){
        public void onClick(final View v){
            switch (v.getId()){
                case R.id.sendButton:
                    onSendButtonClicked();
                    break;
                case R.id.clearButton:
                    onClearButtonClicked();
                    break;
            }
        }
    };

    private void onSendButtonClicked() {
        if(!isEntryValid() || getIsBusy())
            return;

        setIsBusy(true);
        try {
            authenticate();
        }
        catch (Exception e){
            Log.e("ERROR", e.getMessage());
        }
    }

    private void authenticate() {
        IApiService api = ApiService.getApiService();

        LoginInfo loginInfo = new LoginInfo(loginEditText.getText().toString(), passwordEditText.getText().toString());

        Gson gson = new Gson();
        String json = gson.toJson(loginInfo);

        api.authenticate(json).enqueue(new Callback<AuthenticationResponse>() {
            @Override
            public void onResponse(Call<AuthenticationResponse> call, Response<AuthenticationResponse> response) {
                setIsBusy(false);
                if(response.isSuccessful()) {
                    checkAuthorization(response.body());
                }
            }

            @Override
            public void onFailure(Call<AuthenticationResponse> call, Throwable t) {
                setIsBusy(false);
                Log.e(getString(R.string.logErrorTag), getString(R.string.apiAuthorisationConnectionError));
                Toast.makeText(AuthenticationActivity.this, getString(R.string.connectionApiError),
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    private void onClearButtonClicked() {
        clearLoginEntries();
    }

    private void clearLoginEntries() {
        loginEditText.setText(null);
        passwordEditText.setText(null);
    }

    private boolean isEntryValid() {
        boolean isLoginEntryEmpty = TextUtils.isEmpty(loginEditText.getText());
        boolean isPasswordEntryEmpty = TextUtils.isEmpty(passwordEditText.getText());

        if(isLoginEntryEmpty)
            loginEditText.setError(getText(R.string.loginNeeded));

        if(isPasswordEntryEmpty)
            passwordEditText.setError(getText(R.string.passwordNeeded));

        return !isLoginEntryEmpty && !isPasswordEntryEmpty;
    }

    private void checkAuthorization(AuthenticationResponse response){
        if(response.success){

            Toast.makeText(AuthenticationActivity.this, getString(R.string.yourAreConnected), Toast.LENGTH_SHORT).show();

            if(!rememberMeCheckBox.isChecked()){
                clearLoginEntries();
            }

            repositoryService.saveLoginInfo(
                    new LoginInfo(loginEditText.getText().toString(), passwordEditText.getText().toString()));
            repositoryService.saveToken(response.token);
            goToMenuActivity();
        }
        else {
            Toast.makeText(AuthenticationActivity.this, getString(R.string.badLoginOrPassword), Toast.LENGTH_SHORT).show();
        }
    }

    private void goToMenuActivity() {
        Intent intent = new Intent(AuthenticationActivity.this, MenuActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
